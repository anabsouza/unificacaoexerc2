package br.com.itau.Cartao.service;

import br.com.itau.Cartao.gateway.ClienteCartao;
import br.com.itau.Cartao.models.Cartao;

public class CartaoClienteFallback implements ClienteCartao {
    @Override
    public Cartao buscarCartaoPorId(String numero) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(false);
        return cartao;
    }
}
