package br.com.itau.Cartao.DTOs;


import br.com.itau.Cartao.models.Cartao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CartaoEntradaDTO
{
    @NotNull(message =  "Número do cartao não pode ser nulo")
    @NotBlank(message = "Número do cartao não pode ser vazio")
    private String numero;

    @NotNull(message =  "Código do Cliente não pode ser nulo")
    private Long clienteId;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId){
        this.clienteId = clienteId;
    }

    public Cartao converterParaCartao() {
        Cartao cartao = new Cartao();

        cartao.setNumero(this.numero);

        return cartao;
    }

}

